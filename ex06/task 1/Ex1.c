#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

int main(){
	int pd=fork();
	if(pd==0){
		while(1){
			printf("I'm still alive!!!\n");
		}
	}
	else{
		sleep(1);
		printf("Dispatching\n");
		kill(pd, SIGKILL);
		printf("Dispatched\n");
	}
}