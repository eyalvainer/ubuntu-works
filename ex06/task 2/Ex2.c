#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

void quit(int sig);
int main(){
	signal(SIGINT,quit);
	int i;
	for(i=0;i<=2000000000;i++){
		if(i%1000000==0){
			printf("I'm still alive i=%d\n",i);
		}
	}
	exit(EXIT_SUCCESS);
}

void quit(int sig){
	char str[1024];
	puts("Do you really want to close this poor little program?  ");
	gets(str);
	if(str[0]=='Y' || str[0]=='y'){
		exit(1);
	}
}