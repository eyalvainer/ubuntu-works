#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TRUE 1

int count(FILE * fp)
{
    char ch;
    int count=1;
	ch=getc(fp);
    while(TRUE){
		if(ch=='\n')
			count++;
		if(ch==EOF) 
			break;
		ch=getc(fp);
	}   
	//printf("%d",count);
   	return count;
}

int main(int argc, char *argv[])
{
	char line[1024];
	int lineNum;
	int num,i;
	srand(time(NULL));
	if(argc < 1)
	{
		printf("You should enter the file name as a parameter to main!!!  ./hatul <filename>");
		return 0;
	}
	FILE* fp= fopen(argv[1],"r");
	if (fp==NULL)
	{
		printf("\n Wrong file name");
		return 0;
	}
	num=count(fp);
	lineNum = rand()%num;
	fseek(fp,0,SEEK_SET);
	//printf("\n%d",lineNum);
	for (i = 0; i <= lineNum; ++i)
	{
		fgets(line,1024,fp);
	}
	printf("\n %s \n",line);
	fclose(fp);
	return 0;
}
