#include "ceaser.h"


char shift_letter(char letter, int offset)
{
	if(letter+offset>'z'){
		letter=letter-26;
	}
	if(letter+offset<'a'){
		letter=letter+26;
	}
	return letter+offset;
}

char * shift_string(char * input, int offset)
{
	int length = sizeof input;
	int i;
	char temp;
	char encrypted[length];

	for (i = 0; i < length; i++)
		if (input[i] <= 'z' && input[i] >= 'a'){
			temp = shift_letter(input[i],offset);
			encrypted[i]=temp;
		}
	encrypted[i-1] = 00;
	return encrypted;
}