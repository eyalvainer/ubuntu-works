#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "LineParser.h"

#define TRUE 1
#define SIZE 2048

int execute(cmdLine *pCmdLine);	

int main(){
	char input[SIZE];
	char path[MAX_ARGUMENTS];
	cmdLine * pCmdLine;
	getcwd(path,MAX_ARGUMENTS);
	while(TRUE){
		printf("%s >>> ",path);
		fgets(input,SIZE,stdin);

		if(strlen(input)>1)
		{
			
			pCmdLine = parseCmdLines(input);
			execute(pCmdLine);
			getcwd(path,MAX_ARGUMENTS);
		}

	}
	exit(0);
}

int execute(cmdLine *pCmdLine)
{
	if(strcmp(pCmdLine->arguments[0],"cd")==0){
		chdir(pCmdLine->arguments[1]);
		perror("");
		return 0;
	}	
	if(strcmp(pCmdLine->arguments[0],"myecho")==0){
		int i;
		for(i=1;i<pCmdLine->argCount;i++){
			printf("%s",pCmdLine->arguments[i]);
		}
		printf("\n");
		return 0;
	}
	int pd=fork();
	if(pd==0)
	{
		if(pCmdLine->inputRedirect != 0){
			close(0);
			open(pCmdLine->inputRedirect,O_RDONLY);
			if(pCmdLine->outputRedirect != 0){
				close (1);
				open(pCmdLine->outputRedirect,O_WRONLY);
			}
		}
		execvp(pCmdLine->arguments[0],pCmdLine->arguments);
		perror("execvp error");
		exit(1);
	}
	else
	{
		if(pCmdLine->blocking!=0){	
			waitpid(pd,0,0);
		}
	}
}	