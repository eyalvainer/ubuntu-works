#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main(void)
{
        int     fd[2], pd;
        char    string[] = "magshimim";
        char    string2[80];

        pipe(fd);
        
        pd = fork();
        if(pd == 0)
        {
                close(fd[0]);
                write(fd[1], string, strlen(string)+1);
                exit(0);
        }
        else
        {
                close(fd[1]);
                read(fd[0], string2, sizeof(string2));
                printf("Received string: %s\n\n\n", string2);
        }
        
        return(0);
}