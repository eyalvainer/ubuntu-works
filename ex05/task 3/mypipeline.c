#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

int main(){
	int fd[2],pd1,pd2;
	char * command1[]={"ls","-l",NULL};
	char * command2[]={"tail","-n","2",NULL};
	pipe(fd);
	pd1=fork();
	if(pd1==0){
		//close(1);
		close(fd[0]);
		dup2(fd[1],1);
		close(fd[1]);
		execvp(command1[0],command1);
		perror("child 1");
		exit(1);
	}
	else{
		close(fd[1]);
	}
	pd2=fork();
	if(pd2==0){
		//close(0);
		close(fd[1]);
		dup2(fd[0],0);
		close(fd[0]);
		execvp(command2[0],command2);
		perror("child 2");
		exit(1);
	}
	else{
		close(fd[0]);
	}
	waitpid(pd1,0,0);
	waitpid(pd2,0,0);
}