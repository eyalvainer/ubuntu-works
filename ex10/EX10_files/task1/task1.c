#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <elf.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

void main(int argc,char *argv[]){
	if(argc<2)
		exit(0);
	int i=0;
	/*char name[100];
	FILE *file = fopen(argv[1],"r");
	Elf32_Ehdr ehdr;
	Elf32_Shdr shdr;
	fread(&ehdr, sizeof(Elf32_Ehdr),1,file);
	fseek(file,ehdr.e_shoff,SEEK_SET);/////
	for(i=0;i<ehdr.e_shnum;i++){
		
		fread(&shdr, sizeof(Elf32_Shdr),1,file);
		printf("%d %s %d %d %d\n",i,shdr.sh_name,shdr.sh_addr, shdr.sh_offset, shdr.sh_size);
	}*/
	int fd = open(argv[1],O_RDONLY);
	FILE *file = fopen(argv[1],"r");
	fseek(file,0,SEEK_END);
	size_t size = ftell(file); //file size
	fclose(file);
	void *mapped = mmap(0,size,PROT_READ,MAP_PRIVATE,fd,0);
	Elf32_Ehdr *ehdr =(Elf32_Ehdr*) mapped; //casting the mmap output to be ehdr and save it their
	Elf32_Shdr *shdr = (Elf32_Shdr*)(mapped + ehdr->e_shoff); // saves the right thing in shdr
	Elf32_Shdr *stringtable = &shdr[ehdr->e_shstrndx]; //holds the stringtable 
	void *name = mapped + stringtable->sh_offset; //the offset of the stringtable
	for(i=0;i<ehdr->e_shnum;i++){ //e_shnum holds the number of "lines"
		printf("%2d\t%16s\t%8d\t%8d\t%8d\n",i,(name + shdr[i].sh_name),shdr[i].sh_addr, shdr[i].sh_offset, shdr[i].sh_size);
	}
}
