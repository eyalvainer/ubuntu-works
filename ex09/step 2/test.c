#include <stdio.h>
#include <string.h>
#include <unistd.h>
struct metadata_block
{
	size_t size;
	struct metadata_block* next;
	int free;
};
typedef struct metadata_block * p_block;
p_block block=NULL;
void* malloc(size_t increment);
void free(void * ptr);
void * realloc(void *ptr, size_t size2 );
int main(){
	char * str = malloc(10);
	void * ptr = (void *) str;
	void * ptr2;
	
	strcpy(str,"TEST!\0");

	free(str);
	printf("free: OK!\n");

	str = malloc(15);

	if ((void*) str != ptr)
	{
		printf("malloc() should've used the freed memory! old: %p new: %p\n",ptr,str);
		return 1;
	}

	strcpy(str,"test\n\0");
	ptr2 = realloc(str,10);

	malloc(10);
	ptr = realloc(str,20);

	if (ptr2 == ptr)
	{
		printf("realloc() should return a new memory block when expanding memory!\n");
		return 2;
	}
	
	printf("all good in the hood :)\n");
	return 0;
}

void* malloc(size_t increment){
	p_block temp;
	temp = block;
	if(block == NULL){
		block=sbrk(sizeof(struct metadata_block)+increment);
		block->size=increment;
		block->next=NULL;
		block->free=0;
		return (block + sizeof(struct metadata_block));
	}

	while(temp->next != NULL){
		//printf("ggs\n");
		if(temp->free==1 && temp->size <= increment){
			temp->size=increment;
			temp->next=NULL;
			temp->free=0;
					///printf("ggsss\n");

			return(temp+sizeof(struct metadata_block));
		}
		temp=temp->next;
	}

	// in this stage we are already in the lastest piece of pblock so we dont have to loop again
	temp->next=sbrk(increment + sizeof(struct metadata_block));
	temp=temp->next;
	temp->next=NULL;
	temp->free=0;
	temp->size=increment;
	return(temp+sizeof(struct metadata_block));
}

void free(void * ptr){
	//p_block temp = ptr - sizeof(struct metadata_block);
	p_block temp = block;
	p_block prev;
	while(temp->next != NULL && temp != ptr - sizeof(struct metadata_block))	
	{
		prev = (p_block)temp;
		temp = (p_block)(temp->next);
	}
	///prev = (p_block)temp;
	//printf("%p  %p\n",block,temp);
	if(temp->next == NULL ){
		//printf("here2\n");
		temp = sbrk((-1) * (temp->size + sizeof(struct metadata_block)));
		if(prev == NULL){
			block = NULL;
		}
	}
	else{
		temp->free = 1;
	}
}

void * realloc(void *ptr, size_t size2){
	void * temp = ptr;
	void * temp2;
	p_block findsize = block;
	size_t len;
	while(findsize->next != NULL && findsize != ptr - sizeof(struct metadata_block))	
	{
		findsize = (p_block)(findsize->next);
	}
	len = findsize -> size;
	//free(ptr);
	temp2 = malloc(size2);
	free(ptr);
	memcpy(temp2,temp,len);
	return temp2;
}
