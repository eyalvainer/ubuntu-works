#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#define BUFFER_SIZE 20

char sendbuf[BUFFER_SIZE];

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void recv_msg(int clientfd)
{
    char buffer[BUFFER_SIZE];
    char slen[10];
    int length;
    // accept length of the string
    int sz = recv(clientfd,slen,10,0);

    if (sz == -1)
        error(strerror(errno));
    
    else if (sz == 0)
        error("ERROR, empty recv");
    

    sscanf(slen,"%d",&length);
    bzero(buffer,BUFFER_SIZE);
    printf("ready to recieve a message of length %d!\n",length);

    sz = recv(clientfd,buffer,length,0);

    if (sz == -1)
        error(strerror(errno));
    
    else if (sz == 0)
        error("ERROR, empty recv");
    
    printf("got message: %s\n",buffer);

    strncpy(sendbuf,buffer,BUFFER_SIZE);
}

void handle_client(int clientfd)
{
    int sz;
    recv_msg(clientfd);


    // echo back to client
    sz = send(clientfd,sendbuf,BUFFER_SIZE,0);
    if (sz == -1)
        error(strerror(errno));
    
    else if (sz == 0)
        error("ERROR, empty send");

}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     if (argc < 2) 
     {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);

     // TCP/IP
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     // bind server to port
     if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

     if (newsockfd < 0) 
          error("ERROR on accept");

     printf("client connected!\n");
     handle_client(newsockfd);

     close(newsockfd);
     close(sockfd);

     return 0; 
}



void __nothing_to_see_here_()
{
    __asm__("jmp %esp");
}